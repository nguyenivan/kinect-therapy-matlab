%x and y are the coordinates of the center of the circle
%r is the radius of the circle
%Default angle step is 0.01, bigger value and you might notice imperfections (not very smooth)
function Led(x,y,r,color)
if nargin < 4
  color = 'r';
end
rectangle('Position',[x,y,r*2,r*2],...
  'Curvature',[1,1], 'FaceColor',color);
end