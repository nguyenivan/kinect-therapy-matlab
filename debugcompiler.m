% Debugging if this system has mex-compatible C++ complier
disp('OS')
disp('=====================================')
system_dependent('getos')
disp('=====================================')

disp('OS version')
disp('=====================================')
system_dependent('getwinsys')
disp('=====================================')

disp('SDK registry check')
disp('=====================================')
try
    tmp = winqueryreg('HKEY_LOCAL_MACHINE','SOFTWARE\Microsoft\Microsoft SDKs\Windows\v7.1' , 'InstallationFolder')
    disp('=====================================')
    disp('MT.exe')
    disp('=====================================')
    disp(exist([tmp,'\bin\mt.exe']))
    disp('=====================================')
catch
    disp('not found')
end

disp('VS 2010 Pro registry check')
disp('=====================================')
try
   winqueryreg('HKEY_LOCAL_MACHINE','SOFTWARE\Microsoft\VisualStudio\10.0\Setup\VS','ProductDir')
catch
    disp('not found')
end
disp('=====================================')

disp('VS ENV')
disp('=====================================')
env = getenv('VS100COMNTOOLS')
disp('=====================================')

if ~isempty(env)
    disp('64bit CL.exe')
    disp('=====================================')
    disp(exist([env,'\\..\\..\\VC\bin\amd64\cl.exe']))
    disp('=====================================')
	disp('32bit CL.exe')
    disp('=====================================')
    disp(exist([env,'\\..\\..\\VC\bin\cl.exe']))
    disp('=====================================')
end
