Uninstall All Kinect SDK (1.0 and 2.0)

Download OpenNI_NITE_Installer-win64-0.27.zip 
https://code.google.com/p/simple-openni/downloads/detail?name=OpenNI_NITE_Installer-win64-0.27.zip&can=3&q=

Install 
openni-win64-1.5.4.0-dev.msi
nite-win64-1.5.2.21-dev.msi
sensor-win64-5.1.2.1-redist.msi
SensorKinect092-Bin-Win64-v5.1.2.1.msi

If it is Kinect For Windows, follow these steps:
https://groups.google.com/forum/#!topic/openni-dev/ZI2ZmCRDwnA
Download the Kinect bridge from here:
https://www.assembla.com/code/kinect-mssdk-openni-bridge/git/nodes

After install the OpenNI stack, you can plug in the Kinect camera and check if "PrimeSense" group appears in Device Manager. If not, review all the installation steps.

Next step is to install MS SDK 7.1 (Reference: http://www.mathworks.com/matlabcentral/answers/101105-how-do-i-install-microsoft-windows-sdk-7-1)


Download "Visual Studio 2010 Express All-in-One ISO" and install it
http://www.google.com.vn/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0CB0QFjAA&url=http%3A%2F%2Fgo.microsoft.com%2Ffwlink%2F%3FLinkId%3D323467&ei=10xnVMraG8KSmwXg-4DIAg&usg=AFQjCNGH7ySrLPf2lhFk_vhY3AE-jwH7xw&sig2=p_mmnHg8QTX72z5Vm7kc5A&bvm=bv.79142246,d.dGY

Download and install "Visual Studio 2010 SP1"
http://www.google.com.vn/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0CBsQFjAA&url=http%3A%2F%2Fgo.microsoft.com%2Ffwlink%2F%3FLinkId%3D210710&ei=L01nVNX5GKTEmAWWzYDQCg&usg=AFQjCNHd2cMGaKmateBhWv_qPp64C5PfnA&sig2=_F8XnRfSIBRrVP3sKGjEDA

Download and install "Microsoft SDK 7.1"
==>GRMSDK_EN_DVD.iso if you are running x32 Windows 7
==>GRMSDKX_EN_DVD.iso if you are running x64 Windows 7
http://www.microsoft.com/en-us/download/details.aspx?id=8442

Download and install "Microsoft Visual C++ 2010 Service Pack 1 Compiler Update for the Windows SDK 7.1"
http://www.microsoft.com/en-us/download/confirmation.aspx?id=4422

Now you can compile the OpenNI wrapper for Matlab

Download "Kinect Matlab"
http://www.mathworks.com/matlabcentral/fileexchange/30242-kinect-matlab

Extract the content of folder OpenNI1 to C:\Projects\KinectTherapy. Here are the files and folders supposed to be seen:
  compile_cpp_files.m
  Config
  Example
  Example.m
  ExampleCP.m
  ExampleIR.m
  ExampleRS.m
  ExampleRW.m
  ExampleSK.m
  Mex

Go go C:\Projects\KinectTherapy and compile:
  compile_cpp_files

If everything goes well, you will be able to run the examples, try to run the skeleton example:
  ExampleSK