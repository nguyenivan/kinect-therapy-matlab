function ExampleSK()
RECORD = false;
DISPLAY_SKELETON = false;
DISPLAY_RGB = false;
addpath('Mex')
SAMPLE_XML_PATH='Config/SamplesConfig.xml';
global tick;
tick = 0;
global KinectHandles;
global t;
global tempFile;
% Start the Kinect Process
% filename='Example/SkelShort.oni';
% filename='TestCapture.oni';
% KinectHandles=mxNiCreateContext(SAMPLE_XML_PATH,filename);
% To use the Kinect hardware use :

KinectHandles=mxNiCreateContext(SAMPLE_XML_PATH);
t = timer('TimerFcn', @timerCallback, 'Period', 0.1 , 'StartDelay', 1, 'ExecutionMode', 'fixedRate');
c = onCleanup(@CleanItUp);
I=mxNiPhoto(KinectHandles); I=permute(I,[3 2 1]);
h=imshow(I);
hold on
width = size(I,2);
height = size(I,1);
ledsMarginTop = 20;
ledsCount = 20;
ledsRadius =5;
tempFile = 0;
while(1)
    if KinectHandles == 0
        KinectHandles=mxNiCreateContext(SAMPLE_XML_PATH);
    end
    for i=1:ledsCount
        ledX = ((width / ledsCount) * i) + ledsRadius;
        ledY = ledsMarginTop + ledsRadius;
        Led(ledX, ledY, ledsRadius);
    end
    prepTime = 10;
    recordTime = 60;
    tick = 0;
    prepIndex = 0;
    recordIndex = 0;
    hh=zeros(1,9);
    poseName = '';
    while isempty(poseName)
        prompt = {'Enter pose name:'};
        dlg_title = 'Input Parameters';
        num_lines = 1;
        def = {''};
        answer = inputdlg(prompt,dlg_title,num_lines,def);
        if isempty(answer)
            return
        end
        poseName = answer{1};
    end
    tempFile = fopen([poseName '.txt'], 'W');
    tick = 0;
    CaptureHandle = 0;
    frameCount = 0;
    start(t);
    data = [];
    while (1)
        if DISPLAY_RGB
            I=mxNiPhoto(KinectHandles); I=permute(I,[3 2 1]);
            set(h,'Cdata',I);
        end
        Pos= mxNiSkeleton(KinectHandles,1);
        if tick > recordTime + prepTime
            if RECORD
                if CaptureHandle > 0
                    mxNiStopCapture(CaptureHandle);
                end
            end
            break
        elseif tick > prepTime
            frameCount = frameCount + 1;
            if isempty(data)               
                data = reshape(Pos(1:15,3:5)', 1, []);
            else
                data = [data;reshape(Pos(1:15,3:5)', 1, [])];
            end
            if RECORD
                if CaptureHandle == 0
                    CaptureHandle=mxNiStartCapture(KinectHandles,[poseName, '.oni']);
                end
            end
            
            % Draw red led
            nextIndex = int16( (tick - prepTime) / recordTime * ledsCount);
            for i = recordIndex + 1: nextIndex
                ledX = ((width / ledsCount) * i) + ledsRadius;
                ledY = ledsMarginTop + ledsRadius;
                Led(ledX, ledY, ledsRadius, 'y');
            end
            
        else
            % Draw red led
            nextIndex = int16((tick / prepTime) * ledsCount);
            for i = (prepIndex + 1): nextIndex
                ledX = ((width / ledsCount) * i) + ledsRadius;
                ledY = ledsMarginTop + ledsRadius;
                Led(ledX, ledY, ledsRadius, 'g');
            end
            prepIndex = nextIndex;
        end
        mxNiUpdateContext(KinectHandles);
        if(hh(1)>0);
            for i=1:9, delete(hh(i)); end
        end
        
        if DISPLAY_SKELETON
            y=Pos(1:15,7);
            x=Pos(1:15,6);
            
            hh(1)=plot(x,y,'r.');
            hh(2)=plot(x([13 14 15]),y([13 14 15]),'g');
            hh(3)=plot(x([10 11 12]),y([10 11 12]),'g');
            hh(4)=plot(x([9 10]),y([9 10]),'m');
            hh(5)=plot(x([9 13]),y([9 13]),'m');
            hh(6)=plot(x([2 3 4 5]),y([2 3 4 5]),'b');
            hh(7)=plot(x([2 6 7 8]),y([2 6 7 8]),'b');
            hh(8)=plot(x([1 2]),y([1 2]),'c');
            hh(9)=plot(x([2 9]),y([2 9]),'c');
        end
        drawnow
    end
    for i = 1:size(data,1)
        fprintf(tempFile, '%f ', data(i,:));
        fprintf(tempFile,'\n');
    end
    fclose(tempFile);
    tempFile = 0;
    stop(t);
    mxNiDeleteContext(KinectHandles);
    KinectHandles = 0;
    fprintf('Pose %s finished. Frame count: %d\n', poseName, frameCount);
end
end

function timerCallback(~, ~)
global tick;
tick = tick + 0.1;
end

function CleanItUp()
global KinectHandles;
global t;
global tempFile;
if (KinectHandles > 0)
    mxNiDeleteContext(KinectHandles);
    delete(t);
end
if (tempFile > 0)
    fclose(tempFile);
end
end