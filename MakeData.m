function orientations = MakeData(data)
import java.util.*;
addpath('quaternion');

joints = Hashtable;
joints.put('HEAD',1);
joints.put('NECK',2);
joints.put('LEFT_SHOULDER',3);
joints.put('LEFT_ELBOW',4);
joints.put('LEFT_HAND',5);
joints.put('RIGHT_SHOULDER',6);
joints.put('RIGHT_ELBOW',7);
joints.put('RIGHT_HAND',8);
joints.put('TORSO',9);
joints.put('LEFT_HIP',10);
joints.put('LEFT_KNEE',11);
joints.put('LEFT_FOOT',12);
joints.put('RIGHT_HIP',13);
joints.put('RIGHT_KNEE',14);
joints.put('RIGHT_FOOT',15);

angles = Hashtable;
angles.put('NECK', [joints.get('TORSO') joints.get('NECK') joints.get('HEAD')]);
angles.put('LEFT_SHOULDER', [joints.get('LEFT_ELBOW') joints.get('LEFT_SHOULDER') joints.get('NECK')]);
angles.put('LEFT_ELBOW', [joints.get('LEFT_HAND') joints.get('LEFT_ELBOW') joints.get('LEFT_SHOULDER')]);
angles.put('RIGHT_SHOULDER', [joints.get('RIGHT_ELBOW') joints.get('RIGHT_SHOULDER') joints.get('NECK')]);
angles.put('RIGHT_ELBOW', [joints.get('RIGHT_HAND') joints.get('RIGHT_ELBOW') joints.get('RIGHT_SHOULDER')]);
angles.put('LEFT_HIP', [joints.get('LEFT_KNEE') joints.get('LEFT_HIP') joints.get('TORSO')]);
angles.put('LEFT_KNEE', [joints.get('LEFT_FOOT') joints.get('LEFT_KNEE') joints.get('LEFT_HIP')]);
angles.put('RIGHT_HIP', [joints.get('RIGHT_KNEE') joints.get('RIGHT_HIP') joints.get('TORSO')]);
angles.put('RIGHT_KNEE', [joints.get('RIGHT_FOOT') joints.get('RIGHT_KNEE') joints.get('RIGHT_HIP')]);

keys =  ArrayList(angles.keySet( ));

a= reshape(data,3,[])';
ret = [];
iterator = keys.iterator();

while iterator.hasNext()
    key = iterator.next();
    val = angles.get(key);
    vP = a(val(2),:) - a(val(1),:);
    vP = vP / norm(vP);
    vC = a(val(3),:) - a(val(2),:);
    vC = vC / norm(vC);
    q = quaternion.rotateutov(vP, vC);
    if q.real < 0
        q=-1;
    end
    ret = cat(2,ret,q.e');
end
orientations = ret;
end