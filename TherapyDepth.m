function Therapy()
DISPLAY_SKELETON = true;
DISPLAY_RGB = true;
addpath('Mex');
addpath('LibSVM');
SAMPLE_XML_PATH='Config/SamplesConfig.xml';
BUFFER_SIZE = 10;
MODEL_FILE = 'Data/full.model';
POSE_SET = { '45degree', 'handsdown', 'random', 'tpose'};
POSE_ID = [4 1 2];
FEATURE_COUNT = 36;
MATCH_THRESHOLD = 70;
MIN_DISTANCE = 2; % meter
MATCH_DELAY = 1; % second

model = svmloadmodel(MODEL_FILE, FEATURE_COUNT);

global KinectHandles;
global t;
global doMatch;

t = timer('TimerFcn', @timerCallback, 'StartDelay', MATCH_DELAY , 'ExecutionMode', 'singleShot');

% Start the Kinect Process
% filename='Example/SkelShort.oni';
% filename='TestCapture.oni';
% KinectHandles=mxNiCreateContext(SAMPLE_XML_PATH,filename);
% To use the Kinect hardware use :

KinectHandles=mxNiCreateContext(SAMPLE_XML_PATH);
c = onCleanup(@CleanItUp);
I=mxNiPhoto(KinectHandles);
I=permute(I,[3 2 1]);
h=imshow(I);
hold on
width = size(I,2);
height = size(I,1);

D=mxNiDepth(KinectHandles);
D=permute(D,[2 1]);
bitshift(D,-3)
hd = imshow(D, [0 9000]);
colormap('jet')
set(hd, 'Visible', 'off')
hh=zeros(1,9);
poseId = 0;
doMatch = 1;
while (1)
    poseBuffer = zeros(BUFFER_SIZE, FEATURE_COUNT);
    frames = 0;
    labelVector = ones(BUFFER_SIZE,1)*POSE_ID(mod(poseId,3)+1);
    poseId = poseId + 1;
    while (1)
        frames = frames + 1;
        mxNiUpdateContext(KinectHandles);
        if DISPLAY_RGB
            I=mxNiPhoto(KinectHandles); I=permute(I,[3 2 1]);
            set(h,'Cdata',I);
            if ( mod( int16(frames/10), 2)==1)
                D=mxNiDepth(KinectHandles);
                D=permute(D,[2 1]);
                set(hd,'CData',D);
                set(hd, 'Visible', 'on')
            else
                set(hd, 'Visible', 'off');
            end
            
        end
        Pos= mxNiSkeleton(KinectHandles,1);
        data = reshape(Pos(1:15,3:5)', 1, []);
        orientations = MakeData(data);
        orientations(isnan(orientations)) = 0;
        poseBuffer(2:end,:) = poseBuffer(1:end-1,:);
        poseBuffer(1,:) = orientations;
        if doMatch && (min(Pos(1:15,5)) > MIN_DISTANCE)
            [~, a, ~] = svmpredict(labelVector, poseBuffer, model);
            if a(1)> MATCH_THRESHOLD
                gong();
                doMatch = 0;
                if  t.Running == 'off'
                    start(t);
                end
                break;
            end
        end
        
        if(hh(1)>0);
            for i=1:9, delete(hh(i)); end
        end
        
        if DISPLAY_SKELETON && Pos(1) > 0
            y=Pos(1:15,7);
            x=Pos(1:15,6);
            
            hh(1)=plot(x,y,'r.');
            hh(2)=plot(x([13 14 15]),y([13 14 15]),'g');
            hh(3)=plot(x([10 11 12]),y([10 11 12]),'g');
            hh(4)=plot(x([9 10]),y([9 10]),'m');
            hh(5)=plot(x([9 13]),y([9 13]),'m');
            hh(6)=plot(x([2 3 4 5]),y([2 3 4 5]),'b');
            hh(7)=plot(x([2 6 7 8]),y([2 6 7 8]),'b');
            hh(8)=plot(x([1 2]),y([1 2]),'c');
            hh(9)=plot(x([2 9]),y([2 9]),'c');
        end
        drawnow
    end
    
end
stop(t);
mxNiDeleteContext(KinectHandles);
KinectHandles = 0;
end


function timerCallback(~, ~)
global doMatch;
doMatch = 1;
end

function CleanItUp()
global KinectHandles;
global t;
delete(t);
if (KinectHandles > 0)
    mxNiDeleteContext(KinectHandles);
end
end