function qout = UV2q( u, v )
    % function qout = UV2q( u, v )
    % One pair vectors U, V -> one quaternion
    w       = cross( u, v );    % construct vector w perpendicular to u and v
    magw    = norm( w );
    dotuv   = dot( u, v );
    if magw == 0
    % Either norm(u) == 0 or norm(v) == 0 or dotuv/(norm(u)*norm(v)) == 1
        if dotuv >= 0
            qout    = quaternion( 1, 0, 0, 0 );
            return;
        end
    % dotuv/(norm(u)*norm(v)) == -1
    % If v == [v(1); 0; 0], rotate by pi about the [0; 0; 1] axis
        if (v(2) == 0) && (v(3) == 0)
            qout    = quaternion( 0, 0, 0, 1 );
            return;
        end
    % Otherwise constuct "what" such that dot(v,what) == 0, and rotate about it
    % by pi
        what    = [ 0; -v(3); v(2) ]./ sqrt( v(2)^2 + v(3)^2 );
        costh   = -1;
    else
    % Use w as rotation axis, angle between u and v as rotation angle
        what    = w(:) / magw;
        costh   = dotuv /( norm(u) * norm(v) );
    end
    c       = sqrt( 0.5 *( 1 + costh ));    % real element >= 0
    s       = sqrt( 0.5 *( 1 - costh ));
    eout    = [ c; s * what ];
    qout    = [eout(1), eout(2), eout(3), eout(4)];
end