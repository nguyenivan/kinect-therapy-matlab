function ReadNI()
import java.util.*;
addpath('Mex')
addpath('quaternion');
SAMPLE_XML_PATH='Config/SamplesConfig.xml';
prompt = {'Enter data folder:'};
dlg_title = 'Input Parameters';
num_lines = 1;
DEFAULT_DATA_FOLDER = 'Data';
def = {DEFAULT_DATA_FOLDER};
answer = inputdlg(prompt,dlg_title,num_lines,def);
dataFolder = answer{1};
% Start the Kinect Process
POSE_ID = 0;
global KinectHandles;
global tempFile;
global tempFile2;
files = dir([dataFolder '\*.oni']);
outputFile = [dataFolder '\train.txt'];
tempFile = fopen(outputFile,'W');
metaFile =  [dataFolder '\meta.txt'];
tempFile2 = fopen(metaFile,'W');
c = onCleanup(@CleanItUp);
for file = files'
    POSE_ID = POSE_ID + 1;
    fprintf('%d %s\n', POSE_ID, file.name);
    fprintf(tempFile2, '%d %s\n', POSE_ID, file.name);
    KinectHandles=mxNiCreateContext(SAMPLE_XML_PATH,[dataFolder, '\', file.name]);
    joints = Hashtable;  
    joints.put('HEAD',1);
    joints.put('NECK',2);
    joints.put('LEFT_SHOULDER',3);
    joints.put('LEFT_ELBOW',4);
    joints.put('LEFT_HAND',5);
    joints.put('RIGHT_SHOULDER',6);
    joints.put('RIGHT_ELBOW',7);
    joints.put('RIGHT_HAND',8);
    joints.put('TORSO',9);
    joints.put('LEFT_HIP',10);
    joints.put('LEFT_KNEE',11);
    joints.put('LEFT_FOOT',12);
    joints.put('RIGHT_HIP',13);
    joints.put('RIGHT_KNEE',14);
    joints.put('RIGHT_FOOT',15);
    
    angles = Hashtable;
    angles.put('NECK', [joints.get('TORSO') joints.get('NECK') joints.get('HEAD')]);
    angles.put('LEFT_SHOULDER', [joints.get('LEFT_ELBOW') joints.get('LEFT_SHOULDER') joints.get('NECK')]);
    angles.put('LEFT_ELBOW', [joints.get('LEFT_HAND') joints.get('LEFT_ELBOW') joints.get('LEFT_SHOULDER')]);
    angles.put('RIGHT_SHOULDER', [joints.get('RIGHT_ELBOW') joints.get('RIGHT_SHOULDER') joints.get('NECK')]);
    angles.put('RIGHT_ELBOW', [joints.get('RIGHT_HAND') joints.get('RIGHT_ELBOW') joints.get('RIGHT_SHOULDER')]); 
    angles.put('LEFT_HIP', [joints.get('LEFT_KNEE') joints.get('LEFT_HIP') joints.get('TORSO')]);
    angles.put('LEFT_KNEE', [joints.get('LEFT_FOOT') joints.get('LEFT_KNEE') joints.get('LEFT_HIP')]);    
    angles.put('RIGHT_HIP', [joints.get('RIGHT_KNEE') joints.get('RIGHT_HIP') joints.get('TORSO')]);
    angles.put('RIGHT_KNEE', [joints.get('RIGHT_FOOT') joints.get('RIGHT_KNEE') joints.get('RIGHT_HIP')]);
    keys =  ArrayList(angles.keySet( ));


    for i = 1: 10
        iterator = keys.iterator();
        mxNiUpdateContext(KinectHandles);
        Pos= mxNiSkeleton(KinectHandles,1);
        ret = [];
        while iterator.hasNext()
            key = iterator.next();
            val = angles.get(key);
            a = Pos(val, 3:5);
            vP = a(2,:) - a(1,:);
            vP = vP / norm(vP);
            vC = a(3,:) - a(2,:);
            vC = vC / norm(vC);
            q = quaternion.rotateutov(vP, vC);
            if q.real < 0
                q=-1;
            end
            ret = cat(2,ret,q.e');
        end
        fprintf(tempFile, '%d ', POSE_ID);
        for j = 1:length(ret)
            fprintf(tempFile, '%d:%d', j, ret(j));
            if (j<length(ret))
                fprintf(tempFile, ' ');
            end
        end
        fprintf(tempFile, '\n');
    end
    mxNiDeleteContext(KinectHandles);
    KinectHandles = 0;
end
fclose(tempFile);
tempFile = 0;
fclose(tempFile2);
tempFile2 = 0;
end

function CleanItUp()
global KinectHandles;
global tempFile;
if (KinectHandles > 0) 
    mxNiDeleteContext(KinectHandles);
end
if (tempFile > 0)
    fclose(tempFile);
end
if (tempFile2 > 0)
    fclose(tempFile2);
end
end