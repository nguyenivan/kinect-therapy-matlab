% Display skeleton and match posture : T-Pose, 45 Degree and Hands Down
function Therapy()
% Optional settings, just leave them here
DISPLAY_SKELETON = true;
DISPLAY_RGB = true;
% Mex folder has all OpenNI wrapper files
addpath('Mex');
% LibSVM folder has all SVM files for machine learning and prediction
addpath('LibSVM');
% Default config of Kinect
SAMPLE_XML_PATH='Config/SamplesConfig.xml';
% Buffer to capture and test pose
BUFFER_SIZE = 10;
% Model file which I have already trained with my data
MODEL_FILE = 'Data/full.model';
% Labels for Poses
POSE_SET = { '45 DEGREE', 'HANDS DOWN', 'random', 'T-POSE'};
% Sequence of Poses need to be performed by user
POSE_ID = [4 1 2];
% Features for SVM prediction. We take 9 joints (see variable "angles" in
% MakeData.m), each joint we calculate a quaternion corresponding to the
% rotation of two bones from that joint. Each quaternion contains 4 scalars
% w,x,y,z. So the total features number is 9*4 = 36
FEATURE_COUNT = 36;
% If 70% of the buffer are matched against a pose, it is considered matched
MATCH_THRESHOLD = 70;
% Minimum distance from user to camera, required for matching to function
% properly
MIN_DISTANCE = 2; % meter
% This setting is for preventing a pose matched repeatedly
MATCH_DELAY = 1; % second
% Load the already trained model
model = svmloadmodel(MODEL_FILE, FEATURE_COUNT);

% Need to declare global for these, because we have to destroy them in
% onCleanup call back
global KinectHandles;
global t;
global doMatch;

% Every time a pose is matched succesfully, the system will not try to
% match anothe pose until after MATCH_DELAY secons
t = timer('TimerFcn', @timerCallback, 'StartDelay', MATCH_DELAY , 'ExecutionMode', 'singleShot');

% Start the Kinect Process
% filename='Example/SkelShort.oni';
% filename='TestCapture.oni';
% KinectHandles=mxNiCreateContext(SAMPLE_XML_PATH,filename);
% To use the Kinect hardware use :
KinectHandles=mxNiCreateContext(SAMPLE_XML_PATH);
% This is needed crucially because user always press Ctrl-C to exit and if
% we don't clean up KinectHandles, we can't have a second run
c = onCleanup(@CleanItUp);
% Load the first RGB frame and then show it as an image
I=mxNiPhoto(KinectHandles); I=permute(I,[3 2 1]);
h=imshow(I);
width = size(I,2);
height = size(I,1);
% Setup function to display text
pt = PromtText(0,'', width, height);
at = ArmText(0, 'UNKNOWN', width, height);
lt = LegText(0, 'UNKNOWN', width, height);
st = SpineText(0, 'UNKNOWN', width, height);
jointLabels = {'HEAD', 'NECK', 'LEFT SHOULDER', 'LEFT ELBOW', 'LEFT HAND' ...
    'RIGHT SHOULDER', 'RIGHT ELBOW', 'RIGHT HAND', 'TORSO', 'LEFT HIP' ...
    'LEFT KNEE', 'LEFT FOOT', 'RIGHT HIP', 'RIGHT KNEE', 'RIGHT FOOT'};
jt = {};
hold on

hh=zeros(1,9);
poseId = 0;
doMatch = 1;

% This while loop will run repeatedly over the fixed pose sequence: T-Pose,
% 45 Degrees and Hands Down
while (1)
    poseBuffer = zeros(BUFFER_SIZE, FEATURE_COUNT);
    frames = 0;
    labelVector = ones(BUFFER_SIZE,1)*POSE_ID(mod(poseId,3)+1);
    PromtText(pt,POSE_SET{POSE_ID(mod(poseId,3)+1)}, width, height);
    poseId = poseId + 1;
    % This while loop will wait until the current expected pose is matched,
    % then it will "gong" and break in order to move on to the next pose
    while (1)
        frames = frames + 1;
        mxNiUpdateContext(KinectHandles);
        if DISPLAY_RGB
            I=mxNiPhoto(KinectHandles); I=permute(I,[3 2 1]);
            set(h,'Cdata',I);
        end
        % Get the skeleton data from KinectHandles
        Pos= mxNiSkeleton(KinectHandles,1);
        data = reshape(Pos(1:15,3:5)', 1, []);
        % Get 9 quaterions from the Skeleton
        orientations = MakeData(data);
        % Get rid of the NaN
        orientations(isnan(orientations)) = 0;
        % Shift the buffer, ready to insert new data
        poseBuffer(2:end,:) = poseBuffer(1:end-1,:);
        poseBuffer(1,:) = orientations;
        % Check if MIN_DISTANCE is satisfied
        % Then check if the system is at the "Matching State" or still in
        % the MATCH_DELAY period
        if doMatch && (min(Pos(1:15,5)) > MIN_DISTANCE)
            [~, a, ~] = svmpredict(labelVector, poseBuffer, model);
            if a(1)> MATCH_THRESHOLD
                gong();
                doMatch = 0;
                if  t.Running == 'off'
                    % If match, run the timer to do MATCH_DELAY
                    start(t);
                end
                break;
            end
        end
        
     
        
        % Check if there is anything in the Skeleton data, if yes, display
        % it
        if DISPLAY_SKELETON && Pos(1) > 0
                    % Clean up a little

            for i=1:9,
                if (hh(i)>0)
                    delete(hh(i));
                end
            end
            % Remember, player 1 skel data is in row 1:15
            % Column 6 and 7 is for 2D coordinate of the joints
            % (Mapped coordinate)
            y=Pos(1:15,7);
            x=Pos(1:15,6);
            
            % Plot the Bones with different colors
            hh(1)=plot(x,y,'r.');
            hh(2)=plot(x([13 14 15]),y([13 14 15]),'g');
            hh(3)=plot(x([10 11 12]),y([10 11 12]),'g');
            hh(4)=plot(x([9 10]),y([9 10]),'m');
            hh(5)=plot(x([9 13]),y([9 13]),'m');
            hh(6)=plot(x([2 3 4 5]),y([2 3 4 5]),'b');
            hh(7)=plot(x([2 6 7 8]),y([2 6 7 8]),'b');
            hh(8)=plot(x([1 2]),y([1 2]),'c');
            hh(9)=plot(x([2 9]),y([2 9]),'c');
            
            jt = JointText(jt, jointLabels, x,y);
            
            % Calculate the angles and update the text
            ad = rad2deg( GetAngle(Pos(4,3:5) - Pos(3,3:5), Pos(7,3:5) - Pos(6,3:5)));
            ArmText(at, int2str(ad), width, height);
            ld = rad2deg( GetAngle(Pos(11,3:5) - Pos(10,3:5), Pos(14,3:5) - Pos(13,3:5)));
            LegText(lt, int2str(ld), width, height);
            sd = rad2deg( GetAngle(Pos(2,3:5) - Pos(9,3:5), [0 0 1]));
            SpineText(st, int2str(sd), width, height);
            
        end
        
        drawnow
    end
    
end

% This cleaup is very important
stop(t);
mxNiDeleteContext(KinectHandles);
KinectHandles = 0;
end


function timerCallback(~, ~)
global doMatch;
doMatch = 1;
end

function CleanItUp()
global KinectHandles;
global t;
delete(t);
if (KinectHandles > 0)
    mxNiDeleteContext(KinectHandles);
end
end

function h = ArmText(at, degree, maxWidth, maxHeight)
if at == 0
    h = text(0, 0,['Angle Between Arms: ' degree '�'], 'FontSize', 14, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'middle','Color', 'w');
else
    h = at;
    set (h, 'String', ['Angle Between Arms: ' degree '�']);
end
position =get(h,'Extent');
left = 10;
bottom =  10 + position(4);
set (h, 'Position', [left bottom 0]);
end

function h = LegText(lt, degree, maxWidth, maxHeight)
if lt == 0
    h = text(0, 0,['Angle Between Legs: ' degree '�'], 'FontSize', 14, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'middle','Color', 'w');
else
    h = lt;
    set (h, 'String', ['Angle Between Legs: ' degree '�']);
end
position =get(h,'Extent');
left = 10;
bottom =  (10 + position(4))*2;
set (h, 'Position', [left bottom 0]);
end

function h = SpineText(st, degree, maxWidth, maxHeight)
if st == 0
    h = text(0, 0,['Angle Of Spine (And Floor): ' degree '�'], 'FontSize', 14, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'middle','Color', 'w');
else
    h = st;
    set (h, 'String', ['Angle Of Spine (And Floor): ' degree '�']);
end
position =get(h,'Extent');
left = 10;
bottom =  (10 + position(4))*3;
set (h, 'Position', [left bottom 0]);
end

function h = PromtText(pt, pose, maxWidth, maxHeight)
    if pt == 0
        h = text(0, 0,['Next pose: ' pose], 'FontSize', 24, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle','Color', 'w');
    else
        
        h = pt;
        set(h,'String', ['Next pose: ' pose]);
    end 
    position =get(h,'Extent');
    left = maxWidth /2;
    bottom =  maxHeight - 10;
    set (h, 'Position', [left bottom 0]);
    
end

function h = JointText(jt, jl, x, y)
    if isempty(jt)
        l = size(jl,2);
        h = cell(1,l);
        for i = 1:l
            h{i} = text(x(i), y(i), jl(i), 'FontSize', 8, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom','Color', 'g');
        end
    else       
        h = jt;
        l = size(jl,2);
        for i = 1:l
            set(h{i}, 'Position', [x(i), y(i)]);
        end
    end 
end

% Function to get Angle between two vectors
function angle = GetAngle(a,b)
 angle = 2 * atan(norm(a*norm(b) - norm(a)*b) / norm(a * norm(b) + norm(a) * b));
end
